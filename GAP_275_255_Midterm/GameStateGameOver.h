#pragma once
#include "GameState.h"

class GameStateGameOver : public GameState
{
public:
    GameStateGameOver();
    ~GameStateGameOver();

    void Enter(GameStateMachine *gsm) override;
    void Update(SDL_Renderer* ren) override;
    void Exit() override;
};