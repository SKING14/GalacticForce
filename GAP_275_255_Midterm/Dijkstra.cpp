#include "Dijkstra.h"
#include "Terrain.h"
#include "Constants.h"
#include "Map.h"
#include "CollisionShapePoint.h"

#include <vector>
#include <map>
#include <utility>
#include <iterator>

//////////////////////////////////////////////////////////
// Dijkstra's
//////////////////////////////////////////////////////////

void Dijkstra::InitializeSingleSource(Terrain** map, int mapSize, Terrain* startingLocation)
{
    for (int i{ 0 }; i < mapSize; ++i)
    {
        map[i]->SetPath(1000);
    }
    startingLocation->SetPath(0);
}

void Dijkstra::Relax(Terrain* pointA, Terrain* pointB)
{
    if (pointB->GetPath() > pointA->GetPath() + pointB->GetWeight())
    {
        pointB->SetPath(pointA->GetPath() + pointB->GetWeight());
    }
}

void Dijkstra::Dijk(Terrain** map, int mapSize, Terrain* startingLocation, int movement)
{
    // My Variables
    std::vector<Terrain*> routes;
    std::map<int, Terrain*> queue;
    std::map<int, Terrain*>::iterator it;

    // Initialize Elements
    InitializeSingleSource(map, mapSize, startingLocation);

    // Create queue
    for (int i{ 0 }; i < mapSize; ++i)
    {
        queue.insert(std::pair<int, Terrain*>(i, map[i]));
    }

    Terrain* terrainToStore = nullptr;

    // Create Shortest Paths
    while (!queue.empty())
    {
        // checking for smallest weight
        int path = 1001;
        int queueToCheck;
        for (it = queue.begin(); it != queue.end(); ++it)
        {
            if (it->second->GetPath() < path)
            {
                path = it->second->GetPath();
                queueToCheck = it->first;
            }
        }

        it = queue.find(queueToCheck);

        if (queueToCheck - Constants::kwidth >= 0)
            Relax(map[queueToCheck], map[queueToCheck - Constants::kwidth]);
        if (queueToCheck % Constants::kwidth != 0)
            Relax(map[queueToCheck], map[queueToCheck - 1]);
        if (queueToCheck + Constants::kwidth < MapSize)
            Relax(map[queueToCheck], map[queueToCheck + Constants::kwidth]);
        if (queueToCheck % Constants::kwidth != Constants::kwidth - 1)
            Relax(map[queueToCheck], map[queueToCheck + 1]);

        routes.push_back(it->second);
        it->second->Relax(true);
        queue.erase(queueToCheck);
    }

    // Set terrain within range
    for (int j{ 0 }; j < static_cast<int>(routes.size()); ++j)
    {
        if (movement > routes[j]->GetPath())
        {
            routes[j]->SetWithinMovementRange(true);
        }
        else
            routes[j]->SetWithinMovementRange(false);
    }
}

CollisionShapePoint* Dijkstra::AICheckAttackAfterMovement(Terrain** map, int mapSize, Terrain* startingLocation, int movement, Map*& actualMap, int attackRange)
{
    // My Variables
    std::vector<Terrain*> routes;
    std::map<int, Terrain*> queue;
    std::map<int, Terrain*>::iterator it;

    // Initialize Elements
    InitializeSingleSource(map, mapSize, startingLocation);

    // Create queue
    for (int i{ 0 }; i < mapSize; ++i)
    {
        queue.insert(std::pair<int, Terrain*>(i, map[i]));
    }

    Terrain* terrainToStore = nullptr;

    // Create Shortest Paths
    while (!queue.empty())
    {
        // checking for smallest weight
        int path = 1001;
        int queueToCheck;
        for (it = queue.begin(); it != queue.end(); ++it)
        {
            if (it->second->GetPath() < path)
            {
                path = it->second->GetPath();
                queueToCheck = it->first;
            }
        }

        it = queue.find(queueToCheck);

        if (queueToCheck - Constants::kwidth >= 0)
            Relax(map[queueToCheck], map[queueToCheck - Constants::kwidth]);
        if (queueToCheck % Constants::kwidth != 0)
            Relax(map[queueToCheck], map[queueToCheck - 1]);
        if (queueToCheck + Constants::kwidth < MapSize)
            Relax(map[queueToCheck], map[queueToCheck + Constants::kwidth]);
        if (queueToCheck % Constants::kwidth != Constants::kwidth - 1)
            Relax(map[queueToCheck], map[queueToCheck + 1]);

        routes.push_back(it->second);
        it->second->Relax(true);
        queue.erase(queueToCheck);
    }

    // Set terrain within range
    for (int j{ 0 }; j < static_cast<int>(routes.size()); ++j)
    {
        if (movement > routes[j]->GetPath())
        {
            routes[j]->SetWithinMovementRange(true);
        }
        else
            routes[j]->SetWithinMovementRange(false);
    }

    Units* target{ nullptr };
    for (int j{ 0 }; j < static_cast<int>(routes.size()); ++j)
    {
        if (routes[j]->GetWithinMovementRange())
        {
            if (routes[j]->GetOccupied() == nullptr)
            {
                target = actualMap->AIUnitToAttack(attackRange, routes[j]->GetCollisionShape()->GetX() / Constants::ktileSize,
                    routes[j]->GetCollisionShape()->GetY() / Constants::ktileSize);
                if (target != nullptr)
                {
                    CollisionShapePoint* collider = new CollisionShapePoint;
                    collider->SetPoint(routes[j]->GetCollisionShape()->GetX(),
                        routes[j]->GetCollisionShape()->GetY());
                    return collider;
                }
            }
        }
    }
    return nullptr;
}