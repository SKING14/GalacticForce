#pragma once

class CollisionShape;
class CollisionShapePoint;
class CollisionShapeAABB;

// This class checks for collisions based on the type of collision shape
class Collisions
{
private:
    static const int m_koffset{ 10 };  // this is used to offset the point so it doesn't lie 
                                 // on the parameter of the AABB shape (causing it to be in contact with 2 AABB's at once)
public:
    Collisions();
    ~Collisions();

    static bool AreOverlapping(CollisionShape *a, CollisionShape *b);
    static bool PointOverlapAABB(CollisionShapePoint *pt, CollisionShapeAABB *aabb);
};