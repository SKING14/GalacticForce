#pragma once

class Terrain;
class Map;
class CollisionShapePoint;

class Dijkstra
{
private:
    static void InitializeSingleSource(Terrain** map, int mapSize, Terrain * startingLocation);
    static void Relax(Terrain* pointA, Terrain * pointB);
public:
    static void Dijk(Terrain** map, int mapSize, Terrain* startingLocation, int movement);
    static CollisionShapePoint* AICheckAttackAfterMovement(Terrain ** map, int mapSize, Terrain * startingLocation, int movement, Map*& actualMap, int attackRange);
};