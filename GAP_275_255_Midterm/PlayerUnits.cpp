#include "PlayerUnits.h"

PlayerUnits::PlayerUnits(int dstX, int dstY, int srcX, int srcY, int health, int defense, int damage, int attackRange, int movement)
    :Units(dstX, dstY, srcX, srcY, health, defense, damage, attackRange, movement)
{
}

PlayerUnits::~PlayerUnits()
{
}

////////////////////////////////////////////////////////////////
// Input
////////////////////////////////////////////////////////////////

void PlayerUnits::CheckAction(SDL_Keycode keyDown, Map*& map, CollisionShapePoint* target)
{
    switch (keyDown)
    {
    case SDLK_m:
        DisplayMovement(map);
        break;
    case SDLK_a:
        CheckAttackRange(map);
        break;
    case SDLK_d:
        //unitSelected->Defend();
        //unitSelected->SetComplete(true);
        break;
    case SDLK_RETURN:
        ExecuteAction(map, target);
        break;
    default:
        break;
    }
}

// Executes the selected action
void PlayerUnits::ExecuteAction(Map*& map, CollisionShapePoint*& target)
{

    switch (m_currentAction)
    {
    case Actions::MoveAction:
        CheckMove(map, target);
        break;
    case Actions::AttackAction:
        Attack(map, target);
        break;
    default:
        break;
    }
}

void PlayerUnits::AIAttack(Map *& map)
{
}

void PlayerUnits::AIMoveCombo(Map *& map)
{
}

void Units::Defend()
{
    //std::cout << "Units defended!" << std::endl;
}