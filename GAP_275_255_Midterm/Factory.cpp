#include "Factory.h"
#include "Level.h"
#include "Mage.h"
#include "Ranger.h"
#include "Warrior.h"
#include "Map.h"
#include "User.h"
#include "Units.h"
#include "AIUnits.h"
#include "AIRanger.h"
#include "AIWarrior.h"
#include "AIMage.h"

#include <unordered_map>

Factory::Factory()
{
}

Factory::~Factory()
{
    // possible create a member variable vector* to store dynamic memory
}

Level* Factory::LoadLevel(int levelToPlay)
{
    // The levels in the game
    Level* result = new Level(levelToPlay);

    return result;
}

Map* Factory::CreateMap(int map)
{
    std::vector<std::vector<char>> mapOne
    {
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
    };
    
    static std::vector<std::vector<char>> mapTwo
    {
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'w', 'w', 'w', 'w', 'g', 'g', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'g', 'g', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'g', 'g', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'g', 'g', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'g', 'g', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'g', 'g', 'w', 'w', 'w', 'w' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
    };

    
    static std::vector<std::vector<char>> mapThree
    {
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w' },
        { 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
    };

    std::vector<std::vector<char>> mapFour
    {
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
    };
    static std::vector<std::vector<char>> mapFive
    {

        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g' },
        { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g' }, 
    };

    std::vector<std::vector<char>> mapSix
    {
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'r', 'l', 'r', 'l', 'r', 'l', 'r', 'l', 'r', 'l' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'l', 'r', 'l', 'r', 'l', 'r', 'l', 'r', 'l', 'r' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'r', 'l', 'r', 'l', 'r', 'l', 'r', 'l', 'r', 'l' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'l', 'r', 'l', 'r', 'l', 'r', 'l', 'r', 'l', 'r' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
    };

    std::vector<std::vector<char>> mapSeven
    {
        { 'r', 'l', 'r', 'r', 'r', 'r', 'r', 'r', 'l', 'r' },
        { 'l', 'l', 'r', 'r', 'r', 'r', 'r', 'r', 'l', 'l' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'r', 'r', 'r', 'l', 'l', 'l', 'l', 'r', 'r', 'r' },
        { 'r', 'r', 'r', 'l', 'l', 'l', 'l', 'r', 'r', 'r' },
        { 'r', 'r', 'r', 'l', 'l', 'l', 'l', 'r', 'r', 'r' },
        { 'r', 'r', 'r', 'l', 'l', 'l', 'l', 'r', 'r', 'r' },
        { 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r', 'r' },
        { 'l', 'l', 'r', 'r', 'r', 'r', 'r', 'r', 'l', 'l' },
        { 'r', 'l', 'r', 'r', 'r', 'r', 'r', 'r', 'l', 'r' },
    };

    std::unordered_map<int, std::vector<std::vector<char>>> mapSet
    {
        {1, mapOne },
        {2, mapTwo },
        {3, mapThree},
        {4, mapFour},
        {5, mapFive},
        {6, mapSix},
        {7, mapSeven},
    };

    Map* result = new Map(mapSet.at(map));
    return result;
}

std::vector<Units*> Factory::CreateUnits(int units)
{
    //add switch statement here to compare argument with type of Units to create
    std::vector<Units*> LevelOneUnits{ new Warrior(4, 0), new Mage(5, 0), new Ranger(6, 0)};
    std::vector<Units*> LevelTwoUnits{ new Warrior(0, 0), new Mage(4, 0), new Ranger(9, 0) };
    std::vector<Units*> LevelThreeUnits{ new Warrior(0, 0), new Mage(4, 0), new Ranger(9, 0) };
    std::vector<Units*> LevelFourUnits{ new Warrior(0, 5), new Mage(0, 4), new Ranger(0, 6) };
    std::vector<Units*> LevelFiveUnits{ new Warrior(5, 5), new Mage(6, 5), new Ranger(5, 6) };
    std::vector<Units*> LevelSixUnits{ new Warrior(3, 0), new Mage(4, 0), new Ranger(5, 0) };
    std::vector<Units*> LevelSevenUnits{ new Warrior(4, 0), new Mage(4, 9), new Ranger(9, 4) };

    std::unordered_map<int, std::vector<Units*>> Unitset
    {
        {1, LevelOneUnits},
        {2, LevelTwoUnits},
        {3, LevelThreeUnits },
        {4, LevelFourUnits },
        {5, LevelFiveUnits},
        {6, LevelSixUnits },
        {7, LevelSevenUnits},
    };
    
    std::vector<Units*> result = Unitset.at(units);
    return result;
}

std::vector<Units*> Factory::CreateAIUnits(int units)
{
    std::vector<Units*> LevelOneAIUnits{ new AIRanger(4, 9), new AIMage(5, 9), new AIWarrior(6, 9) };
    std::vector<Units*> LevelTwoAIUnits{new AIRanger(0, 9), new AIMage(4, 9), new AIWarrior(9, 9) };
    std::vector<Units*> LevelThreeAIUnits{ new AIRanger(0, 9), new AIMage(4, 9), new AIWarrior(9, 9), new AIRanger(8, 9) };
    std::vector<Units*> LevelFourAIUnits{ new AIRanger(9, 4), new AIMage(9, 5), new AIWarrior(9, 6) };
    std::vector<Units*> LevelFiveAIUnits{ new AIRanger(0, 9), new AIRanger(4, 9), new AIRanger(9, 9), new AIRanger(9, 5), new AIRanger(5, 0), new AIRanger(2, 0) };
    std::vector<Units*> LevelSixAIUnits{ new AIRanger(3, 9), new AIMage(4, 9), new AIWarrior(5, 9) };
    std::vector<Units*> LevelSevenAIUnits{ new AIRanger(0, 0), new AIRanger(9, 0), new AIRanger(0, 9), new AIRanger(9,9), new AIWarrior(0, 4), new AIWarrior(0, 5) };

    std::unordered_map<int, std::vector<Units*>> Unitset
    {
        { 1, LevelOneAIUnits},
        { 2, LevelTwoAIUnits},
        { 3, LevelThreeAIUnits },
        { 4, LevelFourAIUnits },
        { 5, LevelFiveAIUnits },
        { 6, LevelSixAIUnits },
        { 7, LevelSevenAIUnits },
    };

    std::vector<Units*> result = Unitset.at(units);
    return result;
}