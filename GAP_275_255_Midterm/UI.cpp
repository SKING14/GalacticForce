#include "UI.h"

UI::UI(SDL_Renderer *ren)
{
    SetTexture(ren);
}

UI::~UI()
{
	delete m_texture;
	m_texture = nullptr;
	delete m_kfile;
	m_kfile = nullptr;
}

void UI::SetTexture(SDL_Renderer* ren)
{
    m_texture = IMG_LoadTexture(ren, m_kfile);
}

void UI::SetPosition(int x, int y)
{
    m_dst.x = x;
    m_dst.y = y;
}

void UI::Render(SDL_Renderer* ren)
{
    if (!m_hidden)
    {
        SDL_RenderCopy(ren, m_texture, &m_src, &m_dst);
    }
}

void UI::Hide(bool hide)
{
    m_hidden = hide;
}