#include "TitleScreen.h"
#include "SDL_image.h"
#include "Constants.h"

TitleScreen::TitleScreen(SDL_Renderer *ren)
    : m_renderer(ren)
{
    m_titleScreen = IMG_LoadTexture(ren, "resources/TitleScreen.png");
}

TitleScreen::~TitleScreen()
{
}

void TitleScreen::Render()
{
    SDL_Rect src { 0, 0, 800, 450 };
    SDL_Rect dst{ 0, 0, Constants::kwinSize, Constants::kwinSize};
    SDL_RenderCopy(m_renderer, m_titleScreen, &src, &dst);
    SDL_RenderPresent(m_renderer);
}