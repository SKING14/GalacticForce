#pragma once

#include "Units.h"

class PlayerUnits : public Units
{
public:
    PlayerUnits(int dstX, int dstY, int srcX, int srcY, int health, int defense, int damage, int attackRange, int movement);
    virtual ~PlayerUnits();
    void CheckAction(SDL_Keycode keyDown, Map *& map, CollisionShapePoint * target) override;
    void ExecuteAction(Map *& map, CollisionShapePoint *& target);

    void AIAttack(Map *&map) override;
    void AIMoveCombo(Map *&map) override;
};