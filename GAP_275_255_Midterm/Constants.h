#pragma once

#define UnitsSize static_cast<int>(m_units.size())
#define MapLocation ((y * Constants::kwidth) + x)
#define MapSize (Constants::kheight * Constants::kwidth)

struct Constants
{
    static const int kwidth{ 10 };
    static const int kheight{ 10 };
    static const int ktileSize{ 64 };
    static const int kwinSize{ 640 };
};