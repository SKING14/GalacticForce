#include "GameStatePlay.h"

#include <iostream>

#include "Factory.h"
#include "Level.h"
#include "GameStateMachine.h"

GameStatePlay::GameStatePlay(int levelToPlay)
    :m_levelToPlay(levelToPlay)
{
}

GameStatePlay::~GameStatePlay()
{
}

// Selects the next Level
void GameStatePlay::Enter(GameStateMachine* gsm)
{
    GameState::Enter(gsm);
    std::cout << "Entering play." << std::endl;
    m_plevelToPlay = Factory::LoadLevel(m_levelToPlay);
}

// Runs the current Level and changes state when level completed
void GameStatePlay::Update(SDL_Renderer* ren)
{
    if (m_levelToPlay > m_numberOfLevels)
    {
        m_pstateMachine->ChangeState(GameStateMachine::State::GameOver);
        return;
    }
    m_plevelToPlay->Run(ren);
    m_pstateMachine->ChangeState(GameStateMachine::State::Play);
}

//exits the level
void GameStatePlay::Exit()
{
    std::cout << "Exiting play";

}