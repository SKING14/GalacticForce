#pragma once
#include "Units.h"

class Map;

class AIUnits : public Units
{
public:
    AIUnits(int dstX, int dstY, int srcX, int srcY, int health, int defense, int damage, int attackRange, int movement);
    virtual ~AIUnits();
    void AIAttack(Map *&map) override;
    void AIMoveCombo(Map *&map) override;
};