#include "CollisionShapePoint.h"

CollisionShapePoint::CollisionShapePoint()
{
    m_type = Type::Point;
}

CollisionShapePoint::~CollisionShapePoint()
{
}

// Mutator
void CollisionShapePoint::SetPoint(int x, int y)
{
    m_x = x;
    m_y = y;
}

// Accessors
const int CollisionShapePoint::GetX()
{
    return m_x;
}

const int CollisionShapePoint::GetY()
{
    return m_y;
}