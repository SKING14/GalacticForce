#include "User.h"
#include "Map.h"
#include "Units.h"
#include "Collisions.h"
#include "CollisionShapePoint.h"
#include "CollisionShapeAABB.h"
#include "UI.h"

#include <SDL.h>
#include <SDL_image.h>

User::User(std::vector<Units*> units, Map* map)
    : m_units(units)
    , m_map(map)
{
    OccupyMap();
}

User::~User()
{
}

// This converts the units collider from AABB to a point and occupies the location on the map
void User::OccupyMap()
{
    CollisionShapePoint collider;
    for (int i{ 0 }; i < m_units.size(); ++i)
    {
        collider.SetPoint(m_units[i]->GetCollider()->GetX(), m_units[i]->GetCollider()->GetY());
        m_map->Occupy(m_units[i], &collider);
    }
}

// Start the user's turn and return true or false if the user's turn is complete
bool User::TurnStart(SDL_Renderer* ren)
{
    CreateUI(ren);
    Input();
    return TurnEnd();
}

// Checks for any dead units and removes them
bool User::ThrowOutYourDead()
{
    for (int i{ 0 }; i < m_units.size(); ++i)
    {
        if (m_units[i]->Dead())
        {
            m_units[i]->MoveAway(m_map);
            delete m_units[i];
            m_units[i] = nullptr;
            m_units.erase(m_units.begin() + i);
        }
        else
        {
            m_units[i]->SetComplete(false);
        }
    }
    if (m_units.size() <= 0)
    {
        return true;
    }
    else
    {
    return false;
    }
}

// returns true if all the user's units have completed their action for this turn
bool User::TurnEnd()
{
    int unitsCompleted{ 0 };

    for (int i{ 0 }; i < m_units.size(); ++i)
    {
        if (m_units[i]->GetComplete())
        {
            ++unitsCompleted;
        }
    }
    if (unitsCompleted >= m_units.size())
    {
        return true;
    }
    else
    {
        return false;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Render
////////////////////////////////////////////////////////////////////////////////////////////

void User::SetTexture(SDL_Renderer* ren)
{
    m_userTexture = IMG_LoadTexture(ren, "resources/WhiteTile.png");
    SDL_SetTextureAlphaMod(m_userTexture, 100);
}

void User::Render(SDL_Renderer* ren)
{
    SDL_RenderCopy(ren, m_userTexture, &m_userSrc, &m_userDst);
    
    if (m_UI)
    {
        m_UI->Render(ren);  // only if UI is not hidden
    }

    for (int i{ 0 }; i < m_units.size(); ++i)
    {
        m_units[i]->Render(ren);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// User Interface
////////////////////////////////////////////////////////////////////////////////////////////

void User::CreateUI(SDL_Renderer* ren)
{
    if (m_UI == nullptr)
    {
        m_UI = new UI(ren);
    }
}