#include "GameStateMachine.h"

#include "GameStateMenu.h"
#include "GameStateGameOver.h"
#include "GameStatePlay.h"

GameStateMachine::GameStateMachine()
    : m_pCurState(nullptr), m_isPlaying(true)
{
}

GameStateMachine::~GameStateMachine()
{
    delete m_pCurState;
    m_pCurState = nullptr;
}

// Game State Cycle: (1) Exit the previous Game State and call it's method Exit()
// (2) Switch to new Game State
// (3) Enter that game state
void GameStateMachine::ChangeState(State newState)
{
    if (m_pCurState != nullptr)
    {
        m_pCurState->Exit();
        delete m_pCurState;
        m_pCurState = nullptr;
    }
    
    switch (newState)
    {
    case State::Init:
        break;
    case State::Menu:
        m_pCurState = new GameStateMenu();
        break;
    case State::Play:
        ++m_levelToPlay;
        m_pCurState = new GameStatePlay(m_levelToPlay);
        break;
    case State::GameOver:
        m_pCurState = new GameStateGameOver();
        break;
    case State::Exit:
        m_isPlaying = false;
        return;
    }

    m_pCurState->Enter(this);
}

// Calls the Update() for the current Game State
bool GameStateMachine::Update(SDL_Renderer* ren)
{
    m_pCurState->Update(ren);
    return m_isPlaying;
}
