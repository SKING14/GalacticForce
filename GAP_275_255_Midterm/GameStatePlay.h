#pragma once
#include "GameState.h"

class Level;

class GameStatePlay : public GameState
{
private:
    int m_numberOfLevels{ 7 };
    int m_levelToPlay{ 0 };
    Level* m_plevelToPlay{ nullptr };
public:
    GameStatePlay(int levelToPlay);
    ~GameStatePlay();

    void Enter(GameStateMachine* gsm) override;
    void Update(SDL_Renderer* ren) override;
    void Exit() override;
};