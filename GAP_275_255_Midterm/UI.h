#pragma once

#include <SDL.h>
#include <SDL_image.h>

class UI
{
private:
    SDL_Texture* m_texture{ nullptr };
    const char* m_kfile{ "resources/ActionSelection.png" };
    SDL_Rect m_src{ 0, 0, 250, 175 };
    SDL_Rect m_dst{ 0, 0, 128, 128 };
    bool m_hidden{ true };

public:
    UI(SDL_Renderer *ren);
    ~UI();
    void SetTexture(SDL_Renderer * ren);
    void SetPosition(int x, int y);
    void Render(SDL_Renderer* ren);
    void Hide(bool hide);
};