#include "Collisions.h"

#include "CollisionShapePoint.h"
#include "CollisionShapeAABB.h"

Collisions::Collisions()
{
}

Collisions::~Collisions()
{
}

// this takes in any two collider shapes and calls the appropriate collision detection between those types of shapes
bool Collisions::AreOverlapping(CollisionShape *a, CollisionShape *b)
{
    if (a->m_type == CollisionShape::Type::Point)
    {
        CollisionShapePoint *pt = dynamic_cast<CollisionShapePoint*>(a);

        if (b->m_type == CollisionShape::Type::AABB)
        {
            CollisionShapeAABB *aabb = dynamic_cast<CollisionShapeAABB*>(b);
            return PointOverlapAABB(pt, aabb);
        }
    }
    return false;
}

// collision detection for point overlapping AABB
bool Collisions::PointOverlapAABB(CollisionShapePoint *pt, CollisionShapeAABB *aabb)
{
    return ((pt->GetX() >= aabb->GetX()) && (pt->GetX() < aabb->GetX() + aabb->GetW())
        && (pt->GetY() >= aabb->GetY()) && (pt->GetY() < aabb->GetY() + aabb->GetH()));
}