#include "GameState.h"

GameState::GameState()
{
}

GameState::~GameState()
{
}

// This is called by the game state when entering the game state to make sure always pointing towards same game state machine
void GameState::Enter(GameStateMachine *gsm)
{
    m_pstateMachine = gsm;
}