#include "Player.h"
#include "Map.h"
#include "Collisions.h"
#include "UI.h"

Player::Player(std::vector<Units*> units, Map* map)
    : User(units, map)
{
    OccupyMap();
    MarkAsPlayerUnits();
}

Player::~Player()
{
	delete m_map;
    m_map = nullptr;
    delete m_UI;
    m_UI = nullptr;
    delete m_unitSelected;
    m_unitSelected = nullptr;
}

// Marks of the player's units as playerUnits, so they can't attack one another
void Player::MarkAsPlayerUnits()
{
    for (int i{ 0 }; i < UnitsSize; ++i)
    {
        m_units[i]->SetAsPlayerUnit(true);
    }
}

// This loop will continue until the player has completed all his units' actions or ends his turn by choice
void Player::Input()
{
    SDL_Event eve;

    if (SDL_PollEvent(&eve) != 0)
    {
        switch (eve.type)
        {
        case SDL_KEYDOWN:
            CheckKeyboardInput(eve.key.keysym.sym);
            break;
        default:
            break;
        }
    }
}

// Checks the player's input
void Player::CheckKeyboardInput(SDL_Keycode keyDown)
{
    KeyboardMovement(keyDown);
    m_mouseCursor.SetPoint(m_userDst.x, m_userDst.y);
    if ((m_unitSelected) && (!m_unitSelected->GetComplete()))
    {
        m_unitSelected->CheckAction(keyDown, m_map, &m_mouseCursor);
    }
}

// Moves the user's cursor 64-bits (1 tile length) up, down, left, or right
void Player::KeyboardMovement(SDL_Keycode keyDown)
{
    switch (keyDown)
    {
    case SDLK_RIGHT:
        m_userDst.x += Constants::ktileSize;
        break;
    case SDLK_LEFT:
        m_userDst.x -= Constants::ktileSize;
        break;
    case SDLK_UP:
        m_userDst.y -= Constants::ktileSize;
        break;
    case SDLK_DOWN:
        m_userDst.y += Constants::ktileSize;
        break;
    case SDLK_SPACE:
        m_mouseCursor.SetPoint(m_userDst.x, m_userDst.y);
        SelectUnit();
        break;
    default:
        break;
    }
}

// Check the unit the player is trying to select
void Player::SelectUnit()
{
    for (int i = 0; i < UnitsSize; ++i)
    {
        if (Collisions::AreOverlapping(&m_mouseCursor, m_units[i]->GetCollider()) && (!m_units[i]->GetComplete()))
        {
            m_unitSelected = m_units[i];
            m_UI->SetPosition(m_mouseCursor.GetX(), m_mouseCursor.GetY());
            m_UI->Hide(false);
            break;
        }
        else
        {
            m_unitSelected = nullptr;
            m_UI->Hide(true);
            m_map->NoSelection();
        }
    }
}