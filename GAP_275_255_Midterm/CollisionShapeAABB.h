#pragma once
#include "CollisionShape.h"

#include <SDL.h>

class CollisionShapeAABB : public CollisionShape
{
private:
    SDL_Rect m_rect;
public:
    CollisionShapeAABB(int x, int y, int w, int h);
    ~CollisionShapeAABB();

    // Accessors
    int GetX() const;
    int GetY() const;
    int GetW() const;
    int GetH() const;
};