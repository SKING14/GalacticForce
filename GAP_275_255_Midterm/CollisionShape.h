#pragma once

// Parent class for the different collision shapes
class CollisionShape
{
public:
    CollisionShape();
    virtual ~CollisionShape();

    enum class Type
    {
        Point,
        AABB
    };
    Type m_type;
};