#pragma once
#include <SDL.h>

#include "Constants.h"
#include "CollisionShapePoint.h"
#include "CollisionShapeAABB.h" // It won't allow me to forward declare this

class Map;

class Units
{
protected:
    enum Actions
    {
        MoveAction = 'm',
        AttackAction = 'a',
        DefendAction = 'd',
        NoAction = 'n'
    };

    bool m_actionCompleted{ false };
    bool m_isDead{ false };
    char m_currentAction{ NoAction };
    Units* m_pTarget;
    bool m_playerUnit{ false };

    // Stats
    int m_health{};
    int m_defense{};
    int m_damage{};
    int m_attackRange{};
    int m_movement{};

    // source image variables
    const char* mk_pFile{ "resources/Characters.png" };
    const int m_ksrcSize{ 48 };
    const int m_ksrcX;
    const int m_ksrcY;

    // destination image variables
    int m_kdstX;
    int m_kdstY;
    int m_xTilePosition;
    int m_yTilePosition;

    // these variables are used by SDL_RenderCopy inside the method Render()
    SDL_Rect m_Unitsrc{ m_ksrcX, m_ksrcY, m_ksrcSize, m_ksrcSize };
    SDL_Rect m_UnitsDst{ m_kdstX, m_kdstY , Constants::ktileSize, Constants::ktileSize };
    SDL_Texture* m_pUnitsTex;

    // collision detector shape for Units
    CollisionShapeAABB m_collider{ m_kdstX, m_kdstY, Constants::ktileSize, Constants::ktileSize };
public:
    Units(int dstX, int dstY, int srcX, int srcY, int health, int defense, int damage, int attackRange, int movement);
    virtual ~Units();

    virtual void AIAttack(Map *&map);
    virtual void AIMoveCombo(Map *&map);
    virtual void CheckAction(SDL_Keycode keyDown, Map*& map, CollisionShapePoint* target);

    void SetTexture(SDL_Renderer * ren);
    bool GetComplete() const;
    void SetComplete(bool completed);
    CollisionShapeAABB* GetCollider();
    void UpdateCollider();
    void MoveAway(Map *& map);
    void SetAsPlayerUnit(bool isPlayer);
    bool IsPlayerUnit();
    void Render(SDL_Renderer* ren);
    void Move(Map*& map, CollisionShapePoint*& target);
    void Hurt(int damage);
    bool Dead();
    void CheckMove(Map*& map, CollisionShapePoint*& target);
    void Attack(Map*& map, CollisionShapePoint*& target);
    void DisplayMovement(Map*& map);
    void CheckAttackRange(Map*& map);
    void Defend();
};