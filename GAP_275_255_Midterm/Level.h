#pragma once
#include <SDL.h>
#include <vector>

#include "Constants.h"

class User;
class Player;
class AI;
class Units;
class Map;

// Every level of the game should be an instance of this class, includes pointers to the map, Units, and players
class Level
{
private:
    const int m_ksrcSize{ 32 };     /* the length and width of the source image we want to render */
    SDL_Rect m_ksrc;
    SDL_Rect m_kdst;

    Map* m_map;
    Player* m_player;
    AI* m_AI;

    std::vector<Units*> m_units;
    std::vector<Units*> m_AIUnits;

public:
    Level(int levelToLoad);
    ~Level();

    void Run(SDL_Renderer *ren);
    void GenerateTerrain(SDL_Renderer *ren);
    void SpawnGameObjects(SDL_Renderer *ren, std::vector<Units*> units);
    void UserTurn(SDL_Renderer *ren, User* user);
    void Update(SDL_Renderer *ren);
    void Render(SDL_Renderer *ren);
};