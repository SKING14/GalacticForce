#include "AI.h"
#include "Map.h"
#include "Units.h"

#include "Units.h"
#include "Map.h"
#include "CollisionShapePoint.h"
#include "Dijkstra.h"

#include <SDL.h>
#include <SDL_image.h>

AI::AI(std::vector<Units*> units, Map * map)
    : User(units, map)
{
}

AI::~AI()
{
    delete m_map;
    m_map = nullptr;
    delete m_unitSelected;
    m_unitSelected = nullptr;
}

void AI::Input()
{
    for (unsigned int i{ 0 }; i < m_units.size(); ++i)
    {
        m_units[i]->AIAttack(m_map);
        if (!m_units[i]->GetComplete())
        {
            m_units[i]->AIMoveCombo(m_map);
        }
    }
}