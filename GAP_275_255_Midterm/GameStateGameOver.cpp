#include "GameStateGameOver.h"

#include <iostream>

#include "GameStateMachine.h"
#include "GameStateMenu.h"

GameStateGameOver::GameStateGameOver()
{
}

GameStateGameOver::~GameStateGameOver()
{
}

// Enters the game over game state
void GameStateGameOver::Enter(GameStateMachine *gsm)
{
    GameState::Enter(gsm);
    std::cout << "Entering GameOver..." << std::endl;
}

// Confirms with player that they wish to exit the game
void GameStateGameOver::Update(SDL_Renderer* ren)
{
    std::cout << "(e)xit" << std::endl;
    char ch;
    std::cin >> ch;

    // Exits the game
    if (ch == 'e')
    {
        m_pstateMachine->ChangeState(GameStateMachine::State::Exit);
    }
}

// Farewell message
void GameStateGameOver::Exit()
{
    std::cout << "Exiting GameOver..." << std::endl;
}