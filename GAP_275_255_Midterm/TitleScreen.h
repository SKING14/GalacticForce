#pragma once
#include <SDL.h>

class TextBox;

class TitleScreen
{
private:
    SDL_Texture* m_titleScreen;
    SDL_Renderer* m_renderer;
public:
    TitleScreen(SDL_Renderer *ren);
    ~TitleScreen();

    void Render();
};

