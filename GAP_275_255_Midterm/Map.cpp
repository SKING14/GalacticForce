#include "Map.h"
#include <iostream>
#include <conio.h>

#include "Units.h"
#include "Terrain.h"
#include "Dijkstra.h"
#include "CollisionShapePoint.h"
#include "CollisionShapeAABB.h"
#include "Collisions.h"
#include "Constants.h"

#include <SDL.h>
#include <SDL_image.h>
#include <unordered_map>

#define DownAttack (((ypos + attackRange - i) * Constants::kwidth) + xpos)
#define UpAttack (((ypos - attackRange + i) * Constants::kwidth) + xpos)
#define RightAttack ((ypos * Constants::kwidth) + (xpos + attackRange - i)) 
#define LeftAttack ((ypos * Constants::kwidth) + (xpos - attackRange + i))

Map::Map(std::vector<std::vector<char>> map)
{
    SetType(map);
}

Map::~Map()
{
	delete m_map;
	delete m_movementTexture;
	m_movementTexture = nullptr;
	delete m_attackTexture;
	m_attackTexture = nullptr;
	delete m_ksrc;
	m_ksrc = nullptr;
	delete m_moveSrc;
	m_moveSrc = nullptr;
	delete m_dst;
	m_dst = nullptr;

}

////////////////////////////////////////////////////////////////////////////////////
// Initialization
////////////////////////////////////////////////////////////////////////////////////

void Map::SetTexture(SDL_Renderer* ren)
{   
    // Texture for terrain within movement range
    m_movementTexture = IMG_LoadTexture(ren, "resources/BlueTile.png");
    m_attackTexture = IMG_LoadTexture(ren, "resources/RedTile.png");
    SDL_SetTextureAlphaMod(m_movementTexture, 100);
    SDL_SetTextureAlphaMod(m_attackTexture, 100);

    // Normal terrain texture
    for (int y{ 0 }; y < Constants::kheight; ++y)
    {
        for (int x{ 0 }; x < Constants::kwidth; ++x)
        {
            m_map[MapLocation]->SetTexture(ren);
        }
    }
}

// PUT SDL_RECTS IN Header don't want leaking memory
void Map::Render(SDL_Renderer* ren) const
{
    for (int y{ 0 }; y < Constants::kheight; ++y)
    {
        for (int x{ 0 }; x < Constants::kwidth; ++x)
        {
            m_dst->x = Constants::ktileSize* x;
            m_dst->y = Constants::ktileSize* y;
            SDL_RenderCopy(ren, m_map[MapLocation]->GetTexture(), m_ksrc, m_dst);
        }
    }
}

void Map::RenderMovement(SDL_Renderer* ren) const
{
    for (int y{ 0 }; y < Constants::kheight; ++y)
    {
        for (int x{ 0 }; x < Constants::kwidth; ++x)
        {
            m_dst->x = Constants::ktileSize* x;
            m_dst->y = Constants::ktileSize* y;
            if (m_map[MapLocation]->GetWithinMovementRange())
            {
                SDL_RenderCopy(ren, m_movementTexture, m_moveSrc, m_dst);
            }
            if (m_map[MapLocation]->GetWithinAttackRange())
            {
                SDL_RenderCopy(ren, m_attackTexture, m_moveSrc, m_dst);
            }
        }
    }
}

// (1) Unfortunately, I can't have the terrainSet outside of the for loop, because all my terrain*
// will be pointing at the same terrain, which completely messes up Units movement
void Map::SetType(std::vector<std::vector<char>>& map)
{
    for (int y{ 0 }; y < Constants::kheight; ++y)
    {
        for (int x{ 0 }; x < Constants::kwidth; ++x)
        {
            std::unordered_map<char, Terrain*> terrainSet
            {
                { 'g', new Terrain("resources/GrassTile.png", 1, x, y) },
                { 'w', new Terrain("resources/WaterTile.png", 2, x, y) },
                { 'r', new Terrain("resources/RockTile.png", 1, x, y)},
                { 'l', new Terrain("resources/LavaTile.png", 4, x, y)}
            };

            m_map[MapLocation] = terrainSet.at(map[y][x]);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
// Attack Range
////////////////////////////////////////////////////////////////////////////////////

void Map::AttackRange(int attackRange, int xpos, int ypos)
{
    for (int i{ 0 }; i < attackRange; ++i)
    {
        if (DownAttack < MapSize)
            m_map[DownAttack]->SetWithinAttackRange(true);
        if (UpAttack >= 0)
            m_map[UpAttack]->SetWithinAttackRange(true);
        if ((RightAttack < MapSize) && (RightAttack % Constants::kwidth > xpos))
            m_map[RightAttack]->SetWithinAttackRange(true);
        if ((LeftAttack >= 0) && (LeftAttack % Constants::kwidth < xpos))
            m_map[LeftAttack]->SetWithinAttackRange(true);
    }
}

Units* Map::UnitToAttack(CollisionShapePoint *collider)
{
    for (int i{ 0 }; i < MapSize; ++i)
    {
        if (Collisions::AreOverlapping(collider, m_map[i]->GetCollisionShape()))
        {
            return m_map[i]->GetOccupied();
        }
    }
}

Units* Map::AIUnitToAttack(int attackRange, int xpos, int ypos)
{
    Units* target;
    for (int i{ 0 }; i < attackRange; ++i)
    {
        if (DownAttack < MapSize)
        {
            target = IsAttackable(DownAttack);
            if (target != nullptr)
            {
                return target;
            }
        }
        else if (UpAttack >= 0 )
        {
            target = IsAttackable(UpAttack);
            if (target != nullptr)
            {
                return target;
            }
        }
        if ((RightAttack < MapSize) && (RightAttack % Constants::kwidth > xpos))
        {
            target = IsAttackable(RightAttack);
            if (target != nullptr)
            {
                return target;
            }
        }
        if ((LeftAttack >= 0) && (LeftAttack % Constants::kwidth < xpos))
        {
            target = IsAttackable(LeftAttack);
            if (target != nullptr)
            {
                return target;
            }
        }
    }
    return nullptr;
}

Units* Map::IsAttackable(int location)
{
    Units* target;
    {
    if (m_map[location]->GetOccupied() != nullptr)
    {
        target = m_map[location]->GetOccupied();
        if (target->IsPlayerUnit())
            return target;
    } 
    return nullptr;
    }
}

////////////////////////////////////////////////////////////////////////////////////
// Movement Range
////////////////////////////////////////////////////////////////////////////////////

void Map::CheckMovement(int x, int y, int movement)
{
    Dijkstra::Dijk(m_map, MapSize, m_map[MapLocation], movement);
}

CollisionShapePoint* Map::AICheckMovement(int x, int y, int movement, Map*& map, int attackRange)
{
    return Dijkstra::AICheckAttackAfterMovement(m_map, MapSize, m_map[MapLocation], movement, map, attackRange);
}

void Map::NoSelection()
{
    for (int i{ 0 }; i < MapSize; ++i)
    {
        m_map[i]->SetWithinMovementRange(false);
        m_map[i]->SetWithinAttackRange(false);
    }
}

bool Map::WithinMovementRange(CollisionShapePoint* collider)
{
    for (int i{ 0 }; i < MapSize; ++i)
    {
        if ((Collisions::AreOverlapping(collider, m_map[i]->GetCollisionShape())
            && (m_map[i]->GetWithinMovementRange()) && (!m_map[i]->GetOccupied())))
        {
            return true;
        }
    }
    return false;
}

void Map::Occupy(Units* unit, CollisionShapePoint *collider)
{
    for (int i{ 0 }; i < MapSize; ++i)
    {
        if (Collisions::AreOverlapping(collider, m_map[i]->GetCollisionShape()))
        {
            if (!m_map[i]->GetOccupied())
            {
                m_map[i]->OccupySpot(unit);
                NoSelection();
            }
        }
    }
}

void Map::Unoccupy(CollisionShapePoint *collider)
{
    for (int i{ 0 }; i < MapSize; ++i)
        if (Collisions::AreOverlapping(collider, m_map[i]->GetCollisionShape()))
        {
            m_map[i]->UnoccupySpot();
        }
}