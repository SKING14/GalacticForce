#pragma once

#include <SDL.h>
#include <vector>

#include "Constants.h"

class Terrain;
class Grass;
class Water;
class CollisionShapePoint;
class Units;

class Map
{
private:
    Terrain* m_map[100];

    SDL_Texture *m_movementTexture;
    SDL_Texture *m_attackTexture;

    SDL_Rect *m_ksrc = new SDL_Rect{ 0, 0, 32, 32 };
    SDL_Rect *m_moveSrc = new SDL_Rect{ 20, 20, 200, 200 };

    SDL_Rect *m_dst = new SDL_Rect{ 0, 0, Constants::ktileSize, Constants::ktileSize};

public:
    Map(std::vector<std::vector<char>> map);
    ~Map();

    void SetTexture(SDL_Renderer * ren);
    void Render(SDL_Renderer* ren) const;
    void RenderMovement(SDL_Renderer * ren) const;
    void SetType(std::vector<std::vector<char>>& map);
    void CheckMovement(int x, int y, int movement);
    CollisionShapePoint* AICheckMovement(int x, int y, int movement, Map *& map, int attackRange);
    void NoSelection();
    bool WithinMovementRange(CollisionShapePoint * collider);
    void Occupy(Units * unit, CollisionShapePoint * collider);
    Units* UnitToAttack(CollisionShapePoint * collider);
    void Unoccupy(CollisionShapePoint * collider);
    Units* AIUnitToAttack(int attackRange, int xpos, int ypos);
    Units * IsAttackable(int location);
    void AttackRange(int attackRange, int xpos, int ypos);
};