#include "Terrain.h"
#include "Constants.h"
#include "Units.h"

Terrain::Terrain(const char* file, const int weight, int x, int y)
    : mk_pFile(file)
    , mk_pWeight(weight)
{
    m_pCollider = new CollisionShapeAABB{ x * Constants::ktileSize, y * Constants::ktileSize, Constants::ktileSize, Constants::ktileSize };
}

Terrain::~Terrain()
{
	delete m_pTexture;
	m_pTexture = nullptr;
	delete mk_pFile;
	mk_pFile = nullptr;
	delete m_pCollider;
	m_pCollider = nullptr;
	delete m_pOccupyingUnit;
	m_pOccupyingUnit = nullptr;
	delete m_pCollider;
    m_pCollider = nullptr;
}

// Static function that contains all the tiles in the game
void Terrain::SetTexture(SDL_Renderer *ren)
{
    m_pTexture = IMG_LoadTexture(ren, mk_pFile);
}

SDL_Texture* Terrain::GetTexture() const
{
    return m_pTexture;
}

void Terrain::SetPath(int path)
{
    m_shortestPath = path;
}

void Terrain::Relax(bool relax)
{
    m_relaxed = relax;
}

void Terrain::SetWithinMovementRange(bool withinRange)
{
    m_withinMovementRange = withinRange;
}

void Terrain::SetWithinAttackRange(bool withinRange)
{
    m_withinAttackRange = withinRange;
}

void Terrain::UnoccupySpot()
{
    m_pOccupyingUnit = nullptr;
}

void Terrain::OccupySpot(Units * unit)
{
    m_pOccupyingUnit = unit;
}

int Terrain::GetWeight() const
{
    return mk_pWeight;
}

int Terrain::GetPath() const
{
    return m_shortestPath;
}

bool Terrain::GetRelax() const
{
    return m_relaxed;
}

bool Terrain::GetWithinMovementRange() const
{
    return m_withinMovementRange;
}

bool Terrain::GetWithinAttackRange() const
{
    return m_withinAttackRange;
}

CollisionShapeAABB *Terrain::GetCollisionShape() const
{
    return m_pCollider;
}

Units* Terrain::GetOccupied() const
{
    return m_pOccupyingUnit;
}