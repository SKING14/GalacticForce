#pragma once
#include "User.h"
#include <vector>

class Units;
class Map;

class AI : public User
{
public:
    AI(std::vector<Units*> units, Map* map);
    ~AI();

    void Input() override;
};