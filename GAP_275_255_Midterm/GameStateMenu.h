#pragma once
#include "GameState.h"

class TitleScreen;

class GameStateMenu : public GameState
{
private:
    TitleScreen* m_title;
public:
    GameStateMenu();
    ~GameStateMenu();

    void Enter(GameStateMachine *gsm) override;
    void Update(SDL_Renderer* ren) override;
    void Exit() override;
};