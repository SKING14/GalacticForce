#pragma once
#include <SDL_image.h>
#include <SDL.h>
#include "CollisionShapeAABB.h"

class Units;

class Terrain
{
    SDL_Texture* m_pTexture;
    const char* mk_pFile;
    const int mk_pWeight;

    CollisionShapeAABB* m_pCollider{ nullptr };
    Units* m_pOccupyingUnit{ nullptr };

    // movement variables
    int m_shortestPath;
    bool m_relaxed{ false };
    bool m_withinMovementRange{ false };
    bool m_withinAttackRange{ false };

public:
    Terrain(const char* file, const int weight, int x, int y);
    virtual ~Terrain();
    void SetTexture(SDL_Renderer* ren);
    SDL_Texture * GetTexture() const;

    // Mutators
    void SetPath(int path);
    void Relax(bool relax);
    void SetWithinMovementRange(bool withinRange);
    void SetWithinAttackRange(bool withinRange);
    void UnoccupySpot();
    void OccupySpot(Units* unit);

    // Accessors
    int GetWeight() const;
    int GetPath() const;
    bool GetRelax() const;
    bool GetWithinMovementRange() const;
    bool GetWithinAttackRange() const;
    CollisionShapeAABB* GetCollisionShape() const;
    Units* GetOccupied() const;
};