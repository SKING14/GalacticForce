#include "Level.h"
#include "Factory.h"
#include "Map.h"

#include <SDL.h>
#include <SDL_image.h>

// This constructor gets it's map, Units, and users from the factory
Level::Level(int levelToLoad)
{
    m_ksrc = SDL_Rect{ 0, 0, m_ksrcSize, m_ksrcSize};
    m_kdst = SDL_Rect{0, 0, Constants::ktileSize, Constants::ktileSize};
    m_map = Factory::CreateMap(levelToLoad);
    m_units = Factory::CreateUnits(levelToLoad);
    m_AIUnits = Factory::CreateAIUnits(levelToLoad);
    m_player = Factory::CreateUser<Player>(m_units, m_map);
    m_AI = Factory::CreateUser<AI>(m_AIUnits, m_map);
}

Level::~Level()
{
    delete m_map;
    m_map = nullptr;
    delete m_player;
    m_player= nullptr;
    delete m_AI;
    m_AI = nullptr;
}

// Setup level and then call update (main level loop)
void Level::Run(SDL_Renderer *ren)
{
    SDL_RenderClear(ren);
    GenerateTerrain(ren);
    m_player->SetTexture(ren);
    SpawnGameObjects(ren, m_units);
    SpawnGameObjects(ren, m_AIUnits);
    Update(ren);
}

// This function generates the terrain of the level
void Level::GenerateTerrain(SDL_Renderer *ren)
{
    m_map->SetTexture(ren);
}

// Spawns the Units at their starting locations
void Level::SpawnGameObjects(SDL_Renderer *ren, std::vector<Units*> units)
{    
    for (unsigned int i{ 0 }; i < units.size(); ++i)
    {
        units[i]->SetTexture(ren);
    }
}

// Loop for Player and AI, loops until they're turn is complete
void Level::UserTurn(SDL_Renderer *ren, User *user)
{
    bool turnComplete = false;

    user->ThrowOutYourDead();
    while (!turnComplete)
    {
        turnComplete = user->TurnStart(ren);
        Render(ren);
    }
}

// Waits for input from the user and updates
void Level::Update(SDL_Renderer* ren)
{
    bool levelComplete = false;

    while (!levelComplete)
    {
        UserTurn(ren, m_player);
        UserTurn(ren, m_AI);

        if (m_AI->ThrowOutYourDead() || m_player->ThrowOutYourDead())
        {
            levelComplete = true;
        }
    }
}

// Calls the renderers for the objects in the level
void Level::Render(SDL_Renderer* ren)
{
    SDL_RenderClear(ren);
    m_map->Render(ren);
    m_map->RenderMovement(ren);
    m_player->Render(ren);
    m_AI->Render(ren);
    SDL_RenderPresent(ren);
}