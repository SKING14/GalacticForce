#include "GameStateMenu.h"

#include "GameStateMachine.h"
#include "TitleScreen.h"

#include <conio.h>

GameStateMenu::GameStateMenu()
{
}

GameStateMenu::~GameStateMenu()
{
    delete m_title;
    m_title = nullptr;
}

// Title Screen
void GameStateMenu::Enter(GameStateMachine *gsm)
{
    GameState::Enter(gsm);
}

// Checks players input, to start game, options, credits, etc.
void GameStateMenu::Update(SDL_Renderer* ren)
{
    m_title = new TitleScreen(ren);
    m_title->Render();

    _getch();
    m_pstateMachine->ChangeState(GameStateMachine::State::Play);
}

// Exits the Menu
void GameStateMenu::Exit()
{
}