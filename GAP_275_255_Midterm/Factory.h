#pragma once

#include <vector>
#include "AI.h"
#include "Player.h"

class Level;
class Map;
class Units;
class User;


// This factory creates instances for the map and game objects
class Factory
{
public:
    Factory();
    ~Factory();

    static Level* LoadLevel(int levelToPlay);
    static Map* CreateMap(int map);
    static std::vector<Units*> CreateUnits(int Units); // take type here as parameter (i.e. wizard, warrior...)
    static std::vector<Units*> CreateAIUnits(int units);
    static User* CreateUser(std::vector<Units*> units, Map* map);
    
    template<typename T>
    static T* CreateUser(std::vector<Units*> units, Map* map)
    {
        T* result = new T(units, map);
        return result;
    }
};