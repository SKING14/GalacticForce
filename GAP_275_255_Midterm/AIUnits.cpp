#include "AIUnits.h"
#include "Map.h"
#include "CollisionShapePoint.h"


AIUnits::AIUnits(int dstX, int dstY, int srcX, int srcY, int health, int defense, int damage, int attackRange, int movement)
    : Units(dstX, dstY, srcX, srcY, health, defense, damage, attackRange, movement)
{
}

AIUnits::~AIUnits()
{
}

////////////////////////////////////////////////////////////////
// AI
////////////////////////////////////////////////////////////////

void AIUnits::AIAttack(Map*& map)
{
    m_pTarget = map->AIUnitToAttack(m_attackRange, m_xTilePosition, m_yTilePosition);

    if (m_pTarget == nullptr)
    {
        return;
    }
    if (m_pTarget->IsPlayerUnit())
    {
        m_pTarget->Hurt(m_damage);
        m_actionCompleted = true;
        m_pTarget = nullptr;
    }
    else
    {
        m_pTarget = nullptr;
    }
}

void AIUnits::AIMoveCombo(Map*& map)
{
    CollisionShapePoint* collider;

    collider = map->AICheckMovement(m_xTilePosition, m_yTilePosition, m_movement, map, m_attackRange);
    Move(map, collider);
    m_actionCompleted = true;
}