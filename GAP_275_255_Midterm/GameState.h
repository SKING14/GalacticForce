#pragma once
#include <SDL.h>

// Parent class for the different game states
class GameState
{
protected:
    class GameStateMachine* m_pstateMachine;
public:
    GameState();
    virtual ~GameState();

    virtual void Enter(class GameStateMachine* gsm);
    virtual void Update(SDL_Renderer* ren) = 0;
    virtual void Exit() = 0;
};