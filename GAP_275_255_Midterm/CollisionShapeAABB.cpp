#include "CollisionShapeAABB.h"

#include <SDL.h>

CollisionShapeAABB::CollisionShapeAABB(int x, int y, int w, int h)
{
    m_rect = SDL_Rect{ x, y, w, h };
    m_type = Type::AABB;
};

CollisionShapeAABB::~CollisionShapeAABB()
{
}

// Accessors
int CollisionShapeAABB::GetX() const
{
    return m_rect.x;
}

int CollisionShapeAABB::GetY() const
{
    return m_rect.y;
}

int CollisionShapeAABB::GetW() const
{
    return m_rect.w;
}

int CollisionShapeAABB::GetH() const
{
    return m_rect.h;
}
