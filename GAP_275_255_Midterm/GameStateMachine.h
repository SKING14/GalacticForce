#pragma once
#include <SDL.h>

class GameStateMachine
{
private:
    class GameState* m_pCurState{ nullptr };
    bool m_isPlaying{ true };
    int m_levelToPlay{ 0 };
public:
    GameStateMachine();
    ~GameStateMachine();

    enum class State
    {
        Init,
        Menu,
        Play,
        GameOver,
        Exit
    };

    void ChangeState(State newState);
    bool Update(SDL_Renderer* ren);
};