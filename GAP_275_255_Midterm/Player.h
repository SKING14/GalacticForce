#pragma once
#include "User.h"

class Player : public User
{
public:
    Player(std::vector<Units*> units, Map* map);
    ~Player();

    void MarkAsPlayerUnits();
    void Input() override;
    void CheckKeyboardInput(SDL_Keycode keyDown);
    void KeyboardMovement(SDL_Keycode keyDown);
    void SelectUnit();
};