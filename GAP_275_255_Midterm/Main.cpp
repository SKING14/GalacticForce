#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>
#include <conio.h>

#include "GameStateMachine.h"
#include "GameStateGameOver.h"
#include "GameStateMenu.h"
#include "Constants.h"

int main(int argc, char* argv[])
{
    // Initializing the SDL Library for use
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cout << "SDL_Init failure: " << SDL_GetError() << std::endl;
        return 1;
    }

    // Creates the game window
    SDL_Window* win = SDL_CreateWindow("Galactic Force", MapSize, MapSize, Constants::kwinSize, Constants::kwinSize, SDL_WINDOW_SHOWN);
    if (win == nullptr)
    {
        std::cout << "SDL_CreateWindow failure: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }
    
    // Creates the renderer for the game
    SDL_Renderer* ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (ren == nullptr)
    {
        SDL_DestroyWindow(win);
        std::cout << "SDL_CreateRenderer failure: " << SDL_GetError() << std::endl;
        SDL_Quit();
        return 1;
    }

    // Initializing text for use
    TTF_Init();

    _getch();

    // My game
    GameStateMachine game;
    game.ChangeState(GameStateMachine::State::Menu);

    // Main game loop (will be here until the game is over)
    while (game.Update(ren));

    // Clean up code!!
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}