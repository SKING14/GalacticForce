#pragma once
#include "CollisionShape.h"

class CollisionShapePoint : public CollisionShape
{
private:
    int m_x;
    int m_y;
public:
    CollisionShapePoint();
    ~CollisionShapePoint();

    // Mutators
    void SetPoint(int x, int y);

    // Accessors
    const int GetX();
    const int GetY();
};