#include "Units.h"
#include "Map.h"
#include "CollisionShapePoint.h"
#include "Dijkstra.h"

#include <SDL.h>
#include <SDL_image.h>

//  the arguments should be integers based on tile (i.e. 2 for second tile).  
//  when constructed we multiply it by the size we want it to be in game
Units::Units(int dstX, int dstY, int srcX, int srcY, int health, int defense, int damage, int attackRange, int movement)
    : m_xTilePosition(dstX)
    , m_yTilePosition(dstY)
    , m_kdstX(dstX * Constants::ktileSize)
    , m_kdstY(dstY * Constants::ktileSize)
    , m_ksrcX{ srcX * m_ksrcSize }
    , m_ksrcY{ srcY * m_ksrcSize }
{
    m_health = health;
    m_defense = defense;
    m_damage = damage;
    m_attackRange = attackRange;
    m_movement = movement;
}

Units::~Units()
{
	delete m_pTarget;
	m_pTarget = nullptr;
}

////////////////////////////////////////////////////////////////
// Initialization
////////////////////////////////////////////////////////////////

void Units::SetTexture(SDL_Renderer* ren)
{
    m_pUnitsTex = IMG_LoadTexture(ren, mk_pFile);
}

// The Units sprite sheet is 576x384, every 48x48 is a Units tile 12x8 Units tiles
void Units::Render(SDL_Renderer * ren)
{
    SDL_RenderCopy(ren, m_pUnitsTex, &m_Unitsrc, &m_UnitsDst);
}

////////////////////////////////////////////////////////////////
// AI
////////////////////////////////////////////////////////////////

void Units::AIAttack(Map*& map)
{
    m_pTarget = map->AIUnitToAttack(m_attackRange, m_xTilePosition, m_yTilePosition);

    if (m_pTarget == nullptr)
    {
        return;
    }
    if (m_pTarget->IsPlayerUnit())
    {
        m_pTarget->Hurt(m_damage);
        m_actionCompleted = true;
        m_pTarget = nullptr;
    }
    else
    {
        m_pTarget = nullptr;
    }
}

void Units::AIMoveCombo(Map*& map)
{
    CollisionShapePoint* collider;

    collider = map->AICheckMovement(m_xTilePosition, m_yTilePosition, m_movement, map, m_attackRange);

    if (collider != nullptr)
    {
        if (map->WithinMovementRange(collider))
        {
            Move(map, collider);
        }
    }
    
    m_actionCompleted = true;
}

void Units::CheckAction(SDL_Keycode keyDown, Map *& map, CollisionShapePoint * target)
{
}

////////////////////////////////////////////////////////////////
// Movement
////////////////////////////////////////////////////////////////

void Units::CheckMove(Map*& map, CollisionShapePoint*& target)
{
    if (map->WithinMovementRange(target))
    {
        Move(map, target);
        m_actionCompleted = true;
    }
}

void Units::DisplayMovement(Map*& map)
{
    map->NoSelection();      // this clears the map in case there were previous spots displayed

    map->CheckMovement(m_xTilePosition, m_yTilePosition, m_movement);
    m_currentAction = Actions::MoveAction;
}

void Units::Move(Map*& map, CollisionShapePoint*& target)
{
    MoveAway(map);
    m_kdstX = target->GetX();
    m_kdstY = target->GetY();

    UpdateCollider();

    // This truncates the mouse position, making it a multiple of 64 (keeping it "square" with the map)
    m_xTilePosition = target->GetX() / Constants::ktileSize;
    m_yTilePosition = target->GetY() / Constants::ktileSize;

    m_UnitsDst.x = m_xTilePosition * Constants::ktileSize;
    m_UnitsDst.y = m_yTilePosition * Constants::ktileSize;

    map->Occupy(this, target);
}

void Units::MoveAway(Map*& map)
{
    CollisionShapePoint* prevSpot = new CollisionShapePoint;
    prevSpot->SetPoint(m_kdstX, m_kdstY);

    map->Unoccupy(prevSpot);

    delete prevSpot;
    prevSpot = nullptr;
}

////////////////////////////////////////////////////////////////
// Attack
////////////////////////////////////////////////////////////////

void Units::CheckAttackRange(Map*& map)
{
    map->NoSelection();      // this clears the map in case there were previous spots displayed


    map->AttackRange(m_attackRange, m_xTilePosition, m_yTilePosition);
    m_currentAction = Actions::AttackAction;
}

void Units::Hurt(int damage)
{
    m_health = m_health - damage;
    if (m_health <= 0)
    {
        m_isDead = true;
    }
}

void Units::Attack(Map*& map, CollisionShapePoint*& target)
{
    m_pTarget = map->UnitToAttack(target);

    if (m_pTarget == nullptr)
    {
        return;
    }
    else if (!m_pTarget->IsPlayerUnit())
    {
        m_pTarget->Hurt(m_damage);
        m_actionCompleted = true;
        m_pTarget = nullptr;
    }
    else
    {
        m_pTarget = nullptr;
    }
}

////////////////////////////////////////////////////////////////
// Mutators
////////////////////////////////////////////////////////////////

void Units::SetAsPlayerUnit(bool isPlayer)
{
    m_playerUnit = isPlayer;
}

void Units::SetComplete(bool completed)
{
    m_actionCompleted = completed;
}

void Units::UpdateCollider()
{
    m_collider = { m_kdstX, m_kdstY, Constants::ktileSize, Constants::ktileSize };
}

////////////////////////////////////////////////////////////////
// Accessors
////////////////////////////////////////////////////////////////

bool Units::IsPlayerUnit()
{
    return m_playerUnit;
}

CollisionShapeAABB* Units::GetCollider()
{
    return &m_collider;
}

bool Units::GetComplete() const
{
    return m_actionCompleted;
}

bool Units::Dead()
{
    return m_isDead;
}