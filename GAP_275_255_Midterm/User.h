#pragma once
#include <vector>

#include "Units.h"
#include "Constants.h"

class CollisionShapePoint;
class Map;
class UI;

class User
{
protected:
    char m_action;
    std::vector<Units*> m_units{ nullptr };
    CollisionShapePoint m_mouseCursor;
    SDL_Rect m_userSrc { 0, 0, 48, 48 };
    SDL_Rect m_userDst { 0, 0, Constants::ktileSize, Constants::ktileSize};
    SDL_Texture* m_userTexture = nullptr;
    Units *m_unitSelected{ nullptr };
    Map* m_map;
    UI* m_UI;

public:
    User(std::vector<Units*> units, Map* map);
    virtual ~User();

    void OccupyMap();
    bool TurnStart (SDL_Renderer* ren);
    bool ThrowOutYourDead();
    virtual void Input() = 0;
    void SetTexture(SDL_Renderer *ren);
    void Render(SDL_Renderer *ren);
    void CreateUI(SDL_Renderer *ren);
    bool TurnEnd();
};